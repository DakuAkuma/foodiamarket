Данный проект был создан студентом уч. группы 191-331 Шорниковым Андреем.

Дата создания репозитория отлична от даты создания сайта по причине 
позднего ознакомления автора с необходимостью создания репозитория... 

На момент создания репозитория работа над проектом велась в течение недели, а
может и более. (Несколько дней беспрерывного написания кода)

При создании сайта использовалось: HTML 5, CSS 3, JavaScript, 
Bootstrap, jQuery.

p.s. Прошу сильно не бить за то, что так поздно заметил необходимость
в репозитории.